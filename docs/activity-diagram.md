# Activity Diagram

## Original Diagram

```plantuml
@startuml Authentication Flow 

Actor User as user 
box "Auth Context"
Participant "Web UI" as web
Control "Identity Provider" as auth 
Control "REST API" as rest
end box

title Microservice Authentication Flow (Happy Path)

group Login only required once
user -> web : Login Page: username and password
web -> auth : username and password
auth -> auth : validates user credentials
auth -> web : Returns JWT if credentials are valid 
web -> web : Stores the JWT somewhere
end 

group Processes requests with token while valid  
user -> web : Requests secure resource
web -> rest : Presents JWT to RESTFul service
rest -> auth : validates token 
note left 
Sometimes happens on the service 
end note 

auth -> rest : Token Valid
rest -> rest : processes request 
rest -> web : response
web -> user : Presentation
end 

@enduml
```


## Additional Diagram

![Identity Workflow](./IdentityWorkflow.png)
