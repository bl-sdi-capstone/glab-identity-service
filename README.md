# [gLab Identity Service](https://gitlab.com/galvanize-labs/glab-idp-service)

Simple identity provider with Username/Password and JWT authentication.

To install the database, use $PROJECT_ROOT/db_setup.sql.  This will create a 
database with the standard users and roles tables as well as an associative table. 
Script will seed two users, `admin@glab.com` & `user@glab.com`.  Both passwords are 
'password' and should be changed before deployment.

Default login endpoint is `/auth/` which requires either a body with a `username`,
and `password` as a json object, OR a header element `Authorization` with a valid JWT 
token.

Health endpoint, `/actuator/health` is accessible to the public, while any other
actuator endpoint requires the `ADMIN` role.  All actuator endpoints can be configured 
via the application properties, or the config map in K8s.

## Documentation 
- [Activity Diagrams](docs/ActivityDiagrams.md)
- [IdentityService_openapi.json](docs/IdentityService_openapi.json)

## To deploy locally

- Create and install database per instructions above
- Create the following environment variables in the current session
    ```bash
    DB_HOST=localhost:3306
    DB_NAME=identity
    DB_USER=iduser
    DB_PWD=iduser
    JWT_SECRET_KEY=jwtSecretKey
    ```
- Build and run using `./gradlew bootRun` or other method
- Server should be running on port 9100 by default
- For a local install, the Fabric8 Discovery Client will not function.  It is ok to ignore these errors in the log...
    ```java
    io.fabric8.kubernetes.client.KubernetesClientException: Operation: [list]  for kind: [Service]  with name: [null]  in namespace: [null]  failed.
        at io.fabric8.kubernetes.client.KubernetesClientException.launderThrowable(KubernetesClientException.java:64) ~[kubernetes-client-4.13.3.jar:na]
        at io.fabric8.kubernetes.client.KubernetesClientException.launderThrowable(KubernetesClientException.java:72) ~[kubernetes-client-4.13.3.jar:na]
        at io.fabric8.kubernetes.client.dsl.base.BaseOperation.listRequestHelper(BaseOperation.java:167) ~[kubernetes-client-4.13.3.jar:na]
     ...
    ```

## Configuring spring client applications

As this is a "custom" implementation, configuration requires a bit more work.  Client apps will need to add dependencies, and configurations as follows...

### Dependencies
  ```bash
  	implementation 'org.springframework.boot:spring-boot-starter-security'
    implementation group: 'io.jsonwebtoken', name: 'jjwt', version: '0.9.0'
    implementation group: 'javax.xml.bind', name: 'jaxb-api', version: '2.3.1'
  ```

### Configuration 
Follow the example code in [SecurityCredentialsConfig](src/main/java/com/galvanize/security/config/SecurityCredentialsConfig.java),
adjusting for your own implementation. Note the use of [JwtProperties](src/main/java/com/galvanize/security/config/JwtProperties.java).
These properties must be accounted for where applicable.  The following are required for JWT Authentication...

- header - Default `Authentication`
- prefix - Default `Bearer`
- secret - This is system dependent.  MUST be the same as the running IDP server

### Filter
The filter is the class that authenticates the user.  As this method uses JWT
Authentication, your filter should look for the header named `Authentication`, 
and process it as a JWT token, validating it with the secret provided by your
IDP server.  The [JWTTokenAuthenticationFilter](java/com/galvanize/security/filter/JwtTokenAuthenticationFilter.java)
included in this project can be used as is, or customized as necessary.  Note, that
it depends on the [User](java/com/galvanize/model/User.java) model that is 
created upon login as the principle.




