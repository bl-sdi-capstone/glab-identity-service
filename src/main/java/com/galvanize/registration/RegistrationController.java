package com.galvanize.registration;

import com.galvanize.TokenUtils;
import com.galvanize.admin.AdminDao;
import com.galvanize.model.PasswordRequest;
import com.galvanize.model.RegisterRequest;
import com.galvanize.model.User;
import com.galvanize.model.UserDetails;
import com.galvanize.security.config.JwtProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/account")
public class RegistrationController {

    Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private final TokenUtils tokenUtils;
    private final JwtProperties jwtProperties;
    private final RegistrationDAO registrationDAO;
    private final AdminDao adminDao;

    public RegistrationController(TokenUtils tokenUtils, JwtProperties jwtProperties,
                                  RegistrationDAO registrationDAO, AdminDao adminDao) {
        this.tokenUtils = tokenUtils;
        this.jwtProperties = jwtProperties;
        this.registrationDAO = registrationDAO;
        this.adminDao = adminDao;
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void register(@RequestBody RegisterRequest registerRequest){
        registrationDAO.registerUser(registerRequest);
    }

    @PatchMapping("/passwd")
//    @PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void changePassword(@RequestBody PasswordRequest passwordRequest,
                               HttpServletRequest request, @AuthenticationPrincipal User user){

        //Requestor must be the actual user or an administrator
        if(!user.getUsername().equals(passwordRequest.getUsername())){
            throw new IllegalArgumentException();
        }

        registrationDAO.changePassword(passwordRequest);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public UserDetails getUser(@AuthenticationPrincipal User user){
        return adminDao.getUserDetails(user.getUsername());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleUserExists(UsernameAlreadyExistsException ue){
        LOGGER.warn(ue.getMessage(), ue);
        return "Username already exists";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleEmailExists(EmailAlreadyExistsException ee){
        LOGGER.warn(ee.getMessage(), ee);
        return "Email address already registered";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleBadData(IllegalArgumentException e){
        LOGGER.error(e.getMessage(), e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public void handleException(Exception e){
        LOGGER.error(e.getMessage(), e);
    }

}
