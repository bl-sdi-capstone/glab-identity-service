package com.galvanize.model;

import java.util.List;

public class RoleDetails {
    private Role role;
    private List<User> users;

    public RoleDetails(Role role, List<User> users) {
        this.role = role;
        this.users = users;
    }

    public Role getRole() {
        return role;
    }

    public List<User> getUsers() {
        return users;
    }
}
