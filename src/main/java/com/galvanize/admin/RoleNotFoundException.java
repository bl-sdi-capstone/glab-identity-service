package com.galvanize.admin;

public class RoleNotFoundException extends RuntimeException{
    public RoleNotFoundException(String message) {
        super(String.format("Role %s not found", message));
    }
}
