package com.galvanize.admin;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String username) {
        super(String.format("User %s was not found", username));
    }
}
