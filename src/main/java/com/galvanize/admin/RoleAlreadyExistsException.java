package com.galvanize.admin;

public class RoleAlreadyExistsException extends RuntimeException {
    public RoleAlreadyExistsException(String message) {
        super(String.format("Role %s already exists in this context", message));
    }
}
