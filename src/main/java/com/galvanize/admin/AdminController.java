package com.galvanize.admin;

import com.galvanize.model.Role;
import com.galvanize.model.RoleDetails;
import com.galvanize.model.User;
import com.galvanize.model.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping("/api/admin")
public class AdminController {

    Logger logger = LoggerFactory.getLogger(AdminController.class);

    AdminDao adminDao;

    public AdminController(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @GetMapping("/roles")
    public List<Role> getRoles(){
        return adminDao.findAllRoles();
    }

    @GetMapping("/users/{username}")
    public UserDetails getUserDetails(@PathVariable String username){
        return adminDao.getUserDetails(username);
    }
    @PatchMapping("/users/{username}")
    public User updateUser(@PathVariable String username, @RequestBody User user){
        return adminDao.updateUser(username, user);
    }


    @PostMapping("/roles")
    public Role addNewRole(@RequestBody Role role){
        return adminDao.addNewRole(role);
    }

    @PatchMapping("/roles/{rolename}")
    public Role updateRole(@PathVariable String rolename, @RequestBody Role role){
        return adminDao.updateRole(rolename, role);
    }

    @GetMapping("/roles/{rolename}")
    public RoleDetails getRole(@PathVariable String rolename){
        return adminDao.getRoleDetails(rolename);
    }

    @DeleteMapping("/roles/{rolename}")
    public ResponseEntity<String> deleteRole(@PathVariable String rolename){
        adminDao.deleteRole(rolename);
        return ResponseEntity.accepted().body(String.format("Deleted role %s", rolename));
    }

    @PutMapping("/roles/{rolename}/{username}")
    public UserDetails addRoleToUser(@PathVariable String rolename, @PathVariable String username){
        return adminDao.addRoleUser(rolename, username);
    }

    @DeleteMapping("/roles/{rolename}/{username}")
    public UserDetails removeUserFromRole(@PathVariable String rolename, @PathVariable String username){
        return adminDao.removeRoleUser(rolename, username);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e){
        logger.warn(e.getMessage(), e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<String> handleException(RoleAlreadyExistsException e){
        logger.warn(e.getMessage(), e);
        return ResponseEntity.status(409).body(e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<String> handleRoleException(RoleNotFoundException e){
        logger.warn(e.getMessage(), e);
        return ResponseEntity.status(409).body(e.getMessage());
    }
    @ExceptionHandler
    public ResponseEntity<String> handleEmptyResultDataAccessException(EmptyResultDataAccessException e){
        logger.error(e.getMessage(), e);
        return ResponseEntity.noContent().build();
    }
    @ExceptionHandler
    public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException e){
        logger.error(e.getMessage(), e);
        return ResponseEntity.noContent().build();
    }
}
