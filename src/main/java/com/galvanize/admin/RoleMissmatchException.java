package com.galvanize.admin;

public class RoleMissmatchException extends RuntimeException {
    public RoleMissmatchException(String name, String roleName) {
        super(String.format("Role name %s does not match parameter %s"));
    }
}
