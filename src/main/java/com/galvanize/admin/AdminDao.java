package com.galvanize.admin;

import com.galvanize.model.Role;
import com.galvanize.model.RoleDetails;
import com.galvanize.model.User;
import com.galvanize.model.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class AdminDao {

    Logger LOGGER = LoggerFactory.getLogger(AdminDao.class);

    public static final String ROLE_PREFIX = "ROLE_";

    private final JdbcTemplate jdbcTemplate;
    private final BCryptPasswordEncoder passwordEncoder;

    public AdminDao(JdbcTemplate jdbcTemplate, BCryptPasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }

    private static String SELECT_ROLES = "select id, name, description from roles where name like ?";

    private static String SELECT_ROLE_USERS =
            "select u.id, u.username, u.first_name, u.last_name, u.email\n" +
            "from users u, roles r, user_roles ur\n" +
            "where u.id = ur.user_id\n" +
            "  and r.id = ur.role_id\n" +
            "  and r.name like ?";

    private static String SELECT_USER_ROLES =
            "select r.id, r.name, r.description from users u, roles r, user_roles ur " +
            " where r.id = ur.role_id " +
            "and u.id = ur.user_id " +
            "and u.username = ?";


    public List<Role> findAllRoles(){
        return findRoles("%");
    }

    public Role addNewRole(Role role) {
        role.setName(fixRoleName(role.getName()));
        if (roleExists(role.getName())){
            throw new RoleAlreadyExistsException(role.getName());
        }

        jdbcTemplate.update("insert into roles(name, description) " +
                "values(?,?)", role.getName(), role.getDescription());

        List<Role> roles = findRoles(role.getName());
        return roles.isEmpty() ? null : roles.get(0);
    }

    public Role updateRole(String existingRole, Role role) {
        role.setName(fixRoleName(role.getName()));
        existingRole = fixRoleName(existingRole);

        List<Role> roles = findRoles(existingRole);
        if(roles.isEmpty()){
            throw new RoleNotFoundException(existingRole);
        }

        Role updatedRole = roles.get(0);
        if (!role.getName().equals(updatedRole.getName()))
            updatedRole.setName(role.getName());
        if (role.getDescription() != null && !role.getDescription().equals(updatedRole.getDescription()))
            updatedRole.setDescription(role.getDescription());
        else if( updatedRole.getDescription() != null ){
            updatedRole.setDescription(null);
        }

        jdbcTemplate.update("update roles set name = ?, description = ? where id = ?",
                updatedRole.getName(), updatedRole.getDescription(), updatedRole.getId());

        List<Role> newRole = findRoles(role.getName());
        return newRole.isEmpty() ? null : newRole.get(0);

    }

    public RoleDetails getRoleDetails(String role){
        List<Role> roles = findRoles(fixRoleName(role));
        if(roles.isEmpty()){
            return null;
        }

        return new RoleDetails(roles.get(0), findUsersForRole(fixRoleName(role)));
    }

    public void deleteRole(String role){
        List<Role> roles = findRoles(fixRoleName(role));
        if(roles.isEmpty()){
            throw new RoleNotFoundException(fixRoleName(role));
        }
        jdbcTemplate.update("delete from roles where id = ?", roles.get(0).getId());
    }

    public UserDetails addRoleUser(String rolename, String username){
        Role role = findRole(fixRoleName(rolename));
        User user = findUser(username);
        if (role == null || user == null){
            throw new IllegalArgumentException(String.format("User %s or role %s not found", username, rolename));
        }

        jdbcTemplate.update("insert into user_roles(user_id, role_id) values(?,?)", user.getId(), role.getId());

        return getUserDetails(username);
    }

    public UserDetails removeRoleUser(String rolename, String username) {
        Role role = findRole(fixRoleName(rolename));
        User user = findUser(username);
        if (role == null || user == null){
            throw new IllegalArgumentException(String.format("User %s or role %s not found", username, rolename));
        }

        jdbcTemplate.update("delete from user_roles where user_id = ? and role_id = ?", user.getId(), role.getId());

        return getUserDetails(username);
    }

    public List<Role> findRoles(String role) {
        return jdbcTemplate.query(SELECT_ROLES,
                (rs, rowNum) -> new Role(rs.getLong("id"), rs.getString("name"), rs.getString("description")), role);
    }

    public List<User> findUsersForRole(String role){
        return jdbcTemplate.query(SELECT_ROLE_USERS,
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"),
                        rs.getString("first_name"), rs.getString("last_name"), null), role);
    }

    public List<Role> findRolesForUser(String username){
        return jdbcTemplate.query(SELECT_USER_ROLES,
                (rs, rowNum) -> new Role(rs.getLong("id"), rs.getString("name"), rs.getString("description")), username);
    }

    public User findUser(String username){

        return jdbcTemplate.queryForObject("select id, username, first_name, last_name, email, password from users where username = ?",
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"),
                        rs.getString("first_name"), rs.getString("last_name"), rs.getString("password")), username);
    }

    public Role findRole(String role) {
        return jdbcTemplate.queryForObject("select id, name, description from roles where name = ?",
                (rs, rowNum) -> new Role(rs.getLong("id"), rs.getString("name"), rs.getString("description")), fixRoleName(role));
    }

    public boolean roleExists(String roleName){
        return jdbcTemplate.queryForObject(
                "SELECT EXISTS(SELECT 1 FROM roles WHERE name = ?)",
                Boolean.class, roleName);
    }

    public UserDetails getUserDetails(String username) {
        return new UserDetails(findUser(username), findRolesForUser(username));
    }

    private String fixRoleName(String roleName){
        roleName = roleName.toUpperCase();
        if (!roleName.startsWith(ROLE_PREFIX))
            roleName = ROLE_PREFIX + roleName;
        return roleName;
    }

    public User updateUser(String username, User user) {
        User u = findUser(username);
        List<String> mods = new ArrayList<>();
        if (u == null){
            throw new UserNotFoundException(username);
        }
        //User
        if(user.getUsername() != null && !user.getUsername().equals(u.getUsername())) {
            mods.add("username = '"+user.getUsername()+"'");
            username = user.getUsername();
        }
        //First
        if(user.getFirstName() != null && !user.getFirstName().equals(u.getFirstName())) {
            mods.add("first_name = '"+user.getFirstName()+"'");
        }
        //Last
        if(user.getLastName() != null && !user.getLastName().equals(u.getLastName())) {
            mods.add("last_name = '"+user.getLastName()+"'");
        }
        //email
        if(user.getEmail() != null && !user.getEmail().equals(u.getEmail())) {
            mods.add("email = '"+user.getEmail()+"'");
        }
        //password
        if(user.getPassword() != null) {
            if(!passwordEncoder.matches(user.getPassword(), u.getPassword())) {
                String passwd = passwordEncoder.encode(user.getPassword());
                u.setPassword(passwd);
                mods.add("password = '"+passwd+"'");
            }
        }

        if(!mods.isEmpty()) {
            StringBuilder SQL = new StringBuilder();
            SQL.append("update users set ");
            Iterator<String> it = mods.iterator();
            while (it.hasNext()) {
                SQL.append(it.next());
                if (it.hasNext())
                    SQL.append(", ");
                else
                    SQL.append(" ");
            }
            SQL.append("where id = ").append(u.getId()).append(";");
            LOGGER.info("Update User SQL IS: " + SQL);

            jdbcTemplate.update(SQL.toString());

        }

        return findUser(username);
    }
}
