package com.galvanize.security.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SqlResolve")
@Service   // It has to be annotated with @Service.
public class UserDetailsServiceImpl implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private JdbcTemplate jdbcTemplate;

    public UserDetailsServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return findUserByUserName(username);

    }

    public UserPrinciple findUserByUserName(String username) throws UsernameNotFoundException {
        UserPrinciple userPrinciple = null;
        try {
            logger.debug(String.format("Looking up user %s", username));
            userPrinciple = jdbcTemplate.queryForObject(
                    "select id, first_name, last_name, username,  email, password from users where username = ?",
                    (rs, rowNum) -> new UserPrinciple(rs.getLong("id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("username"),
                            rs.getString("password"),
                            rs.getString("email")),
                    username);
        }catch (EmptyResultDataAccessException e){
            logger.info(String.format("User %s NOT found", username));
            throw new UsernameNotFoundException("Username: " + username + " not found");
        }

        if(userPrinciple != null){
            List<String> roles = jdbcTemplate.query("select r.name from users u, user_roles ur, roles r where u.id = ur.user_id and ur.role_id = r.id and u.username = ? ",
                    (rs, rowNum) -> rs.getString("name")
                    , username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            for(Object role : roles) {
                authorities.add(new SimpleGrantedAuthority(role.toString()));
            }
            userPrinciple.setAuthorities(authorities);
            logger.debug(String.format("User %s found\n%s", userPrinciple.getUsername(), userPrinciple));
            return userPrinciple;
        }else{
            logger.debug(String.format("User %s NOT found", username));
            // If user not found. Throw this exception.
            throw new UsernameNotFoundException("Username: " + username + " not found");
        }
    }

}