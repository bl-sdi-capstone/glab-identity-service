package com.galvanize.security.config;

import com.galvanize.security.filter.JwtTokenAuthenticationFilter;
import com.galvanize.security.filter.JwtUsernameAndPasswordAuthenticationFilter;
import com.galvanize.security.user.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity 	// Enable security config. This annotation denotes config for spring security.
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityCredentialsConfig extends WebSecurityConfigurerAdapter {

//    @Qualifier("userDetailsServiceImpl")
    private UserDetailsServiceImpl userDetailsService;
    private JwtProperties jwtProperties;

    public SecurityCredentialsConfig(UserDetailsServiceImpl userDetailsService, JwtProperties jwtProperties) {
        this.userDetailsService = userDetailsService;
        this.jwtProperties = jwtProperties;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.cors();
        http
                .cors().and()
                .csrf().disable()
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // handle an authorized attempts
                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                .addFilterBefore(new JwtTokenAuthenticationFilter(jwtProperties), UsernamePasswordAuthenticationFilter.class)
                // Add a filter to validate user credentials and add token in the response header
                // What's the authenticationManager()?
                // An object provided by WebSecurityConfigurerAdapter, used to authenticate the user passing user's credentials
                // The filter needs this auth manager to authenticate the user.
                .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), userDetailsService, jwtProperties))
                .authorizeRequests()
                // HEALTH
                .antMatchers(HttpMethod.GET, "/actuator/health").permitAll()
                // LOGIN
                .antMatchers(HttpMethod.POST, jwtProperties.getUri()).permitAll()
                // REGISTER
                .antMatchers(HttpMethod.POST, "/api/account/register/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/api/users/**").hasRole("USER")
//                .antMatchers(HttpMethod.GET, "/api/user/**").permitAll()
                // CHANGE PASSWORD
//                .antMatchers(HttpMethod.PUT, "/password/**").hasRole("USER")
                // ADMIN ACTUATOR ENDPOINTS (ALL ARE EXPOSED)
//                .antMatchers(HttpMethod.GET,"/actuator/**", "/api/admin/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/api/admin/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/api/admin/**").hasRole("ADMIN")
                // any other requests must be authenticated
                .anyRequest().authenticated();
    }

    // Spring has UserDetailsService interface, which can be overriden to provide our implementation for fetching user from database (or any other source).
    // The UserDetailsService object is used by the auth manager to load the user from database.
    // In addition, we need to define the password encoder also. So, auth manager can compare and verify passwords.
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtProperties getJwtProperties(){
        return new JwtProperties();
    }

    /*
     * Basic CORS configuration. This is a bean so it can managed by Spring and injected where needed.
     * Note: There are many ways to configure CORS in your security configuration.
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOriginPattern("*");
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setExposedHeaders(Arrays.asList("*","Authorization"));
        configuration.setAllowCredentials(true);
        configuration.applyPermitDefaultValues();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
